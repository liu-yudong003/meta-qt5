# meta-qt5



## 项目名称

为聚元PolyOS新增嵌入式图形化支持

## 项目描述

聚元PolyOS主要基于RISCV架构和面向嵌入式场景，当前社区没有针对RISCV架构的图形化支持，因此本项目的目标是依托聚元PolyOS社区和其他开源社区定制基于Qt5的图形化支持meta-layer，并在QEMU或者visionfive2上进行演示。

## 主要工作

1.定制生成支持图形化的meta-qt5

2.目标机器上自带example测试程序

3.在QEMU或者visionfive2上进行演示

## 技术要求

熟悉Yocto框架、原理

熟悉嵌入式Linux开发


## 项目状态

待开发
